﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace StackExchangeSearcher.UI.Converters {
	public class DateTimeOffsetToStringConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null)
				return "unknown";

			var obj = (DateTimeOffset) value;
			return obj.ToString("dd MM yy") + " at " + obj.ToString("HH:mm");
		}
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			return null;
		}
	}
}