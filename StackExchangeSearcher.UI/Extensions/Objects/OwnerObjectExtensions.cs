﻿using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Extensions.Objects {
	public static class OwnerObjectExtensions {
		public static OwnerModel ToModel(this OwnerObject obj) {
			return new OwnerModel {
				AcceptRate = obj.accept_rate,
				DisplayName = obj.display_name,
				Link = obj.link,
				ProfileImage = obj.profile_image,
				Reputation = obj.reputation,
				UserId = obj.user_id,
				UserType = obj.user_type
			};
		}
	}
}