﻿using System.Collections.ObjectModel;
using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Extensions.Objects {
	public static class SearchObjectExtensions {
		public static SearchModel ToModel(this SearchObject obj) {
			return new SearchModel {
				Items = new ObservableCollection<QuestionModel>(obj.items.ToModels()),
				HasMore =  obj.has_more
			};
		}
	}
}