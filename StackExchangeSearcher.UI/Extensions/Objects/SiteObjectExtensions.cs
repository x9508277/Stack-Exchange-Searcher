﻿using System.Collections.Generic;
using System.Linq;
using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Extensions.Objects {
	public static class SiteObjectExtensions {
		public static IEnumerable<SiteModel> ToModels(this IEnumerable<SiteObject> objs) {
			return objs.Select(ToModel).ToArray();
		}

		public static SiteModel ToModel(this SiteObject obj) {
			return new SiteModel {
				ApiSiteParameter = obj.api_site_parameter,
				IconUrl = obj.icon_url,
				Name = obj.name
			};
		}
	}
}