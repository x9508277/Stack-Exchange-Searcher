﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Extensions.Objects {
	public static class QuestionObjectExtensions {
		public static IEnumerable<QuestionModel> ToModels(this IEnumerable<QuestionObject> objs) {
			return objs.Select(ToModel).ToArray();
		}

		public static QuestionModel ToModel(this QuestionObject obj) {
			return new QuestionModel {
				AcceptedAnswerId = obj.accepted_answer_id,
				AnswerCount = obj.answer_count,
				LastActivityDate = DateTimeOffset.FromUnixTimeSeconds(obj.last_activity_date),
				CreationDate = DateTimeOffset.FromUnixTimeSeconds(obj.creation_date),
				IsAnswered = obj.is_answered,
				Link = obj.link,
				Owner = obj.owner.ToModel(),
				QuestionId = obj.question_id,
				Score = obj.score,
				Tags = obj.tags,
				ViewCount = obj.view_count,
				Title = obj.title,
				BodyMarkdown = obj.body_markdown
			};
		}
	}
}