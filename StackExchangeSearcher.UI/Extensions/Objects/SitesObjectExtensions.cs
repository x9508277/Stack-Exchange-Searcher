﻿using System.Collections.ObjectModel;
using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Extensions.Objects {
	public static class SitesObjectExtensions {
		public static SitesModel ToModel(this SitesObject obj) {
			return new SitesModel {
				Items = new ObservableCollection<SiteModel>(obj.items.ToModels())
			};
		}
	}
}