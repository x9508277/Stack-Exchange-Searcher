﻿using System.Threading.Tasks;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Helpers {
	public interface IWebHelper {
		Task<SitesModel> GetSitesTaskAsync();
		Task<SearchModel> GetSearchResultsTaskAsync(string searchText, string sort, string site, int page = 1);
	}
}