﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchangeSearcher.UI.Extensions.Objects;
using StackExchangeSearcher.UI.JsonObjects;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.Helpers.Realizations {
	public class WebHelper : IWebHelper {
		private RequestModel request;
		

		public Task<SitesModel> GetSitesTaskAsync() {
			return Task.Run(async () => {
				var handler = new HttpClientHandler {
					AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
				};
				using (var client = new HttpClient(handler)) {
					client.DefaultRequestHeaders.Add("Host", "api.stackexchange.com");
					client.DefaultRequestHeaders.Add("Connection", "keep-alive");

					using (var result = await client.GetAsync("http://api.stackexchange.com/2.2/sites?filter=!*L1)ih4ub*LWS)TP").ConfigureAwait(true)) {
						var r = await result.Content.ReadAsStringAsync().ConfigureAwait(true);

						try {
							var json = JsonConvert.DeserializeObject<SitesObject>(r);
							return json.ToModel();
						}
						catch (Exception e) {
							var logger = NLog.LogManager.GetCurrentClassLogger();
							logger.Error(e);
							return null;
						}
					}
				}
			});
		}

		public Task<SearchModel> GetSearchResultsTaskAsync(string searchText, string sort, string site, int page = 1) {
			return Task.Run(async () => {
				var handler = new HttpClientHandler {
					AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
				};
				using (var client = new HttpClient(handler)) {
					client.DefaultRequestHeaders.Add("Host", "api.stackexchange.com");
					client.DefaultRequestHeaders.Add("Connection", "keep-alive");

					request = new RequestModel(page.ToString(), "50", "desc", sort, searchText, site);
					using (var result = await client.GetAsync(request.ToString()).ConfigureAwait(true)) {
						var r = await result.Content.ReadAsStringAsync().ConfigureAwait(true);
						try {
							var json = JsonConvert.DeserializeObject<SearchObject>(r);
							return json.ToModel();
						}
						catch (Exception e) {
							var logger = NLog.LogManager.GetCurrentClassLogger();
							logger.Error(e);
							return null;
						}
					}
				}
			});
		}
	}
}