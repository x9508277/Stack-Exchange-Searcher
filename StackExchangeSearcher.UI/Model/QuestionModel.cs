﻿using System;
using System.Net;
using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class QuestionModel : ViewModelBase {
		private string[] tags;
		public string[] Tags {
			get => tags;
			set => Set(() => Tags, ref tags, value);
		}

		private OwnerModel owner;
		public OwnerModel Owner {
			get => owner;
			set => Set(() => Owner, ref owner, value);
		}

		private bool isAnswered;
		public bool IsAnswered {
			get => isAnswered;
			set => Set(() => IsAnswered, ref isAnswered, value);
		}

		private int viewCount;
		public int ViewCount {
			get => viewCount;
			set => Set(() => ViewCount, ref viewCount, value);
		}

		private int acceptedAnswerId;
		public int AcceptedAnswerId {
			get => acceptedAnswerId;
			set => Set(() => AcceptedAnswerId, ref acceptedAnswerId, value);
		}

		private int answerCount;
		public int AnswerCount {
			get => answerCount;
			set => Set(() => AnswerCount, ref answerCount, value);
		}

		private int score;
		public int Score {
			get => score;
			set => Set(() => Score, ref score, value);
		}

		private DateTimeOffset lastActivityDate;
		public DateTimeOffset LastActivityDate {
			get => lastActivityDate;
			set => Set(() => LastActivityDate, ref lastActivityDate, value);
		}

		private DateTimeOffset creationDate;
		public DateTimeOffset CreationDate {
			get => creationDate;
			set => Set(() => CreationDate, ref creationDate, value);
		}

		private int questionId;
		public int QuestionId {
			get => questionId;
			set => Set(() => QuestionId, ref questionId, value);
		}

		private string bodyMarkdown;
		public string BodyMarkdown {
			get => bodyMarkdown;
			set {
				var val = value.Length >= 400 ? value.Substring(0, 400) + "..." : value;
				val = WebUtility.HtmlDecode(val);
				val = val.Replace("\n", "").Replace("\r", "");
				Set(() => BodyMarkdown, ref bodyMarkdown, val);
			}
		}

		private string link;
		public string Link {
			get => link;
			set => Set(() => Link, ref link, value);
		}

		private string title;
		public string Title {
			get => title;
			set {
				var val = value.Length >= 100 ? value.Substring(0, 100) + "..." : value;
				val = WebUtility.HtmlDecode(val);
				val = val.Replace("\n", "").Replace("\r", "");
				Set(() => Title, ref title, val);
			}
		}
	}
}