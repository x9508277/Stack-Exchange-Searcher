﻿using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class SortModel : ViewModelBase {
		private string name;
		public string Name {
			get => name;
			set => Set(() => Name, ref name, value);
		}

		private string description;
		public string Description {
			get => description;
			set => Set(() => Description, ref description, value);
		}
	}
}