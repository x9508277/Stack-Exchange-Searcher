﻿using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class OwnerModel : ViewModelBase {
		private int reputation;
		public int Reputation {
			get => reputation;
			set => Set(() => Reputation, ref reputation, value);
		}

		private int userId;
		public int UserId {
			get => userId;
			set => Set(() => UserId, ref userId, value);
		}

		private string userType;
		public string UserType {
			get => userType;
			set => Set(() => UserType, ref userType, value);
		}

		private int acceptRate;
		public int AcceptRate {
			get => acceptRate;
			set => Set(() => AcceptRate, ref acceptRate, value);
		}

		private string profileImage;
		public string ProfileImage {
			get => profileImage;
			set => Set(() => ProfileImage, ref profileImage, value);
		}

		private string displayName;
		public string DisplayName {
			get => displayName;
			set => Set(() => DisplayName, ref displayName, value);
		}

		private string link;
		public string Link {
			get => link;
			set => Set(() => Link, ref link, value);
		}
	}
}