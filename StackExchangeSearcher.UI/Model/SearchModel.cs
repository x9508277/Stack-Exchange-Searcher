﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class SearchModel : ViewModelBase {
		private ObservableCollection<QuestionModel> items;
		public ObservableCollection<QuestionModel> Items {
			get => items;
			set => Set(() => Items, ref items, value);
		}

		private bool hasMore;
		public bool HasMore {
			get => hasMore;
			set => Set(() => HasMore, ref hasMore, value);
		}
	}
}