﻿using System.Net;
using System.Text;
using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class RequestModel : ViewModelBase {
		public readonly string url = "http://api.stackexchange.com/2.2/search?";
		public readonly string filter = "!9Z(-wwK4f";

		public RequestModel(string page, string pageSize, string order, string sort, string initle, string site) {
			Page = page;
			PageSize = pageSize;
			Order = order;
			Sort = sort;
			Intitle = initle;
			Site = site;
		}

		private string page;
		public string Page {
			get => page;
			set => Set(() => Page, ref page, value);
		}

		private string pageSize;
		public string PageSize {
			get => pageSize;
			set => Set(() => PageSize, ref pageSize, value);
		}

		private string order;
		public string Order {
			get => order;
			set => Set(() => Order, ref order, value);
		}

		private string sort;
		public string Sort {
			get => sort;
			set => Set(() => Sort, ref sort, value);
		}

		private string intitle;
		public string Intitle {
			get => intitle;
			set => Set(() => Intitle, ref intitle, value);
		}

		private string site;
		public string Site {
			get => site;
			set => Set(() => Site, ref site, value);
		}

		public override string ToString() {
			var builder = new StringBuilder();
			builder.Append(url);
			builder.Append("page=");
			builder.Append(Page);
			builder.Append("&pagesize=");
			builder.Append(PageSize);
			builder.Append("&order=");
			builder.Append(Order);
			builder.Append("&sort=");
			builder.Append(Sort);
			builder.Append("&intitle=");
			builder.Append(WebUtility.UrlEncode(Intitle));
			builder.Append("&site=");
			builder.Append(Site);
			builder.Append("&filter=");
			builder.Append(filter);
			return builder.ToString();
		}
	}
}