﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class SitesModel : ViewModelBase {
		private ObservableCollection<SiteModel> items;
		public ObservableCollection<SiteModel> Items {
			get => items;
			set => Set(() => Items, ref items, value);
		}
	}
}