﻿using GalaSoft.MvvmLight;

namespace StackExchangeSearcher.UI.Model {
	public class SiteModel : ViewModelBase {
		private string apiSiteParameter;
		public string ApiSiteParameter {
			get => apiSiteParameter;
			set => Set(() => ApiSiteParameter, ref apiSiteParameter, value);
		}

		private string name;
		public string Name {
			get => name;
			set => Set(() => Name, ref name, value);
		}

		private string iconUrl;
		public string IconUrl {
			get => iconUrl;
			set => Set(() => IconUrl, ref iconUrl, value);
		}
	}
}