﻿namespace StackExchangeSearcher.UI.JsonObjects {
	public class QuestionObject {
		public string[] tags { get; set; }

		public OwnerObject owner { get; set; }

		public bool is_answered { get; set; }

		public int view_count { get; set; }

		public int accepted_answer_id { get; set; }

		public int answer_count { get; set; }

		public int score { get; set; }

		public long last_activity_date { get; set; }

		public long creation_date { get; set; }

		public int question_id { get; set; }

		public string body_markdown { get; set; }

		public string link { get; set; }

		public string title { get; set; }
	}
}