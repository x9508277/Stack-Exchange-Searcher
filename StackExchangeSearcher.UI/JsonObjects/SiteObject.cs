﻿namespace StackExchangeSearcher.UI.JsonObjects {
	public class SiteObject {
		public string api_site_parameter { get; set; }

		public string name { get; set; }

		public string icon_url { get; set; }
	}
}