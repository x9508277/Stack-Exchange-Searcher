﻿using System.Collections.ObjectModel;

namespace StackExchangeSearcher.UI.JsonObjects {
	public class SitesObject {
		public ObservableCollection<SiteObject> items { get; set; }
	}
}