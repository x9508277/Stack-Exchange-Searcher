﻿using System.Collections.ObjectModel;

namespace StackExchangeSearcher.UI.JsonObjects {
	public class SearchObject {
		public ObservableCollection<QuestionObject> items { get; set; }

		public bool has_more { get; set; }
	}
}