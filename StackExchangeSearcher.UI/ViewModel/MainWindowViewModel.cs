﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MaterialDesignThemes.Wpf;
using StackExchangeSearcher.UI.Helpers;
using StackExchangeSearcher.UI.Model;

namespace StackExchangeSearcher.UI.ViewModel {
	public class MainWindowViewModel : ViewModelBase {
		private string requestUrl = "http://api.stackexchange.com/2.2/search?page=1&pagesize=50&order=desc&sort=activity&intitle=java&site=stackoverflow&filter=!9Z(-wwK4f";
		private string searchTextTemp;
		private int currentPage;

		private readonly IWebHelper webHelper;

		public MainWindowViewModel(IWebHelper webHelper) {
			this.webHelper = webHelper;
		}

		private bool isSearching;
		public bool IsSearching {
			get => isSearching;
			set => Set(() => IsSearching, ref isSearching, value);
		}

		private string searchText;
		public string SearchText {
			get => searchText;
			set => Set(() => SearchText, ref searchText, value);
		}

		private SearchModel search;
		public SearchModel Search {
			get => search;
			set => Set(() => Search, ref search, value);
		}

		private SitesModel sites;
		public SitesModel Sites {
			get => sites;
			set => Set(() => Sites, ref sites, value);
		}

		private SiteModel selectedSite;
		public SiteModel SelectedSite {
			get => selectedSite;
			set => Set(() => SelectedSite, ref selectedSite, value);
		}

		private ObservableCollection<SortModel> sorts = new ObservableCollection<SortModel> {
			new SortModel {
				Name = "activity",
				Description = "Last activity"
			},
			new SortModel {
				Name = "creation ",
				Description = "Creation"
			},
			new SortModel {
				Name = "votes",
				Description = "Votes"
			},
			new SortModel {
				Name = "relevance",
				Description = "Relevance (site)"
			},
		};
		public ObservableCollection<SortModel> Sorts {
			get => sorts;
			set => Set(() => Sorts, ref sorts, value);
		}

		private SortModel selectedSort;
		public SortModel SelectedSort {
			get => selectedSort;
			set => Set(() => SelectedSort, ref selectedSort, value);
		}

		private int currentPageDisplay;
		public int CurrentPageDisplay {
			get => currentPageDisplay;
			set => Set(() => CurrentPageDisplay, ref currentPageDisplay, value);
		}

		private SnackbarMessageQueue messageQueue = new SnackbarMessageQueue();
		public SnackbarMessageQueue MessageQueue {
			get => messageQueue;
			set => Set(() => MessageQueue, ref messageQueue, value);
		}

		private ICommand loadedCommand;
		public ICommand LoadedCommand => loadedCommand ?? (loadedCommand =
			new RelayCommand(async () => {
				Sites = await webHelper.GetSitesTaskAsync().ConfigureAwait(true);
				SelectedSite = Sites.Items[0];
			}));

		private ICommand searchCommand;
		public ICommand SearchCommand => searchCommand ?? (searchCommand =
			new RelayCommand(async () => {
				IsSearching = true;
				searchTextTemp = searchText;
				Search?.Items?.Clear();
				Search = await webHelper.GetSearchResultsTaskAsync(SearchText, SelectedSort.Name, SelectedSite.ApiSiteParameter).ConfigureAwait(true);
				IsSearching = false;

				if (Search?.Items == null || !Search.Items.Any()) {
					MessageQueue.Enqueue("Nothing found");
					return;
				}

				currentPage = 1;
				CurrentPageDisplay = currentPage;
			}, () => !string.IsNullOrEmpty(SearchText) && SelectedSite != null && SelectedSort != null));

		private ICommand nextPageCommand;
		public ICommand NextPageCommand => nextPageCommand ?? (nextPageCommand =
			new RelayCommand(() => {
				currentPage++;
				CurrentPageDisplay = currentPage;
				ChangePageAsync(currentPage);
			}, () => Search != null && Search.HasMore));

		private ICommand previousPageCommand;
		public ICommand PreviousPageCommand => previousPageCommand ?? (previousPageCommand =
			new RelayCommand(() => {
				currentPage--;
				CurrentPageDisplay = currentPage;
				ChangePageAsync(currentPage);
			}, () => currentPage > 1));

		private ICommand changePageCommand;
		public ICommand ChangePageCommand => changePageCommand ?? (changePageCommand =
			new RelayCommand(() => {
				ChangePageAsync(CurrentPageDisplay);
				currentPage = CurrentPageDisplay;
			}, () => CurrentPageDisplay > 0 && CurrentPageDisplay < 100));

		private ICommand clearTextCommand;
		public ICommand ClearTextCommand => clearTextCommand ?? (clearTextCommand =
			new RelayCommand(() => {
				SearchText = "";
				searchTextTemp = searchText;
			}));

		private ICommand openQuestionCommand;
		public ICommand OpenQuestionCommand => openQuestionCommand ?? (openQuestionCommand =
			new RelayCommand<string>(url => Process.Start(url)));

		private ICommand openHomeSiteCommand;
		public ICommand OpenHomeSiteCommand => openHomeSiteCommand ?? (openHomeSiteCommand =
			new RelayCommand(() => Process.Start("https://stackexchange.com/")));

		private async void ChangePageAsync(int page) {
			IsSearching = true;
			Search?.Items?.Clear();
			Search = await webHelper.GetSearchResultsTaskAsync(searchTextTemp, SelectedSort.Name, SelectedSite.ApiSiteParameter, page).ConfigureAwait(true);
			IsSearching = false;

			if (Search?.Items != null && Search.Items.Any())
				return;

			currentPage = 0;
			CurrentPageDisplay = currentPage;
			MessageQueue.Enqueue("Nothing found");
		}
	}
}