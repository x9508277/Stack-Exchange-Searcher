using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using StackExchangeSearcher.UI.Helpers;
using StackExchangeSearcher.UI.Helpers.Realizations;
using StackExchangeSearcher.UI.ViewModel;

namespace StackExchangeSearcher.UI.Locators {
	public class ViewModelLocator {
		public ViewModelLocator() {
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

			SimpleIoc.Default.Register<MainWindowViewModel>();
			SimpleIoc.Default.Register<IWebHelper, WebHelper>();
		}

		public MainWindowViewModel MainWindowViewModel => ServiceLocator.Current.GetInstance<MainWindowViewModel>();

		public static void Cleanup() {
			// TODO Clear the ViewModels
		}
	}
}